window.onload = Ext.onReady(function () {
  window.tour = new Tour({
    steps: [{
      element: "#dashboard",
      title: "1st Tour",
      description: "This is Dashboard!",
      position: "left"
    }, {
      element: "#report-bug",
      title: "2nd Tour",
      description: "Click Here to Report Bug",
      position: "left"
    }, {
      element: "#widget-one",
      title: "1st Widget",
      description: "This box is Widget One!",
      position: "right"
    }, {
      element: "#widget-two",
      title: "2nd Widget",
      description: "This box is Widget Two!",
      position: "right"
    }, {
      element: "#default-btn",
      title: "Default Button",
      description: "this is a Default Button!",
      position: "right"
    }, {
      element: "#primary-btn",
      title: "Primary",
      description: "this is a primary Button",
      position: "right"
    }]
  })
  tour.override('showStep', function (self, step) {
    self(step);
  })
  let task = new Ext.util.DelayedTask(function () {
    tour.start();
  });
  task.delay(1000)
})
